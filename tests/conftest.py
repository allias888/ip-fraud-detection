import time

import pytest
from redis import StrictRedis

from app import views
from app.views import app as flask_app
from db.models import Users, db as _db
from app.views import update


TEST_DATABASE_URI = 'postgresql://demo:demo@127.0.0.1/test_ip'


@pytest.fixture(scope='session')
def app(request):
    """
    Session-wide test `Flask` application
    """
    settings_override = {
        'TESTING': True,
        'SQLALCHEMY_DATABASE_URI': TEST_DATABASE_URI
    }

    flask_app.config.update(settings_override)

    ctx = flask_app.app_context()
    ctx.push()

    def teardown():
        ctx.pop()

    request.addfinalizer(teardown)
    return flask_app


@pytest.fixture(scope='function')
def db(app, request):
    """
    Session-wide test database
    """

    _db.app = app
    _db.create_all()

    def teardown():
        _db.drop_all()

    request.addfinalizer(teardown)
    return _db


@pytest.fixture(scope='function')
def session(db, request):
    """
    Creates a new database session for a test
    """
    connection = db.engine.connect()
    transaction = connection.begin()

    options = dict(bind=connection, binds={})
    session = db.create_scoped_session(options=options)

    db.session = session

    def teardown():
        transaction.rollback()
        connection.close()
        session.remove()

    request.addfinalizer(teardown)
    return session


@pytest.yield_fixture(scope='function')
def test_redis():
    """
    Change redis client in update module
    """
    redis = StrictRedis(host='localhost', port=6379, db=1)
    views.redis = redis
    yield redis
    redis.flushdb()

def update_redis_data():
    """
    Function to update redis structures from db rows
    Need to wait some interval to "collect" data
    """
    interval = 0.1
    time.sleep(interval)
    update(interval)

@pytest.yield_fixture()
def simple_users(session, test_redis):
    """
    Users have same subnets but different ips in it
    """
    _id, _id2 = 1, 2
    users = [Users(user_id=_id, ip='1.1.1.4'), Users(user_id=_id, ip='2.2.2.4'),
             Users(user_id=_id2, ip='1.1.1.1'), Users(user_id=_id2, ip='2.2.2.2')]
    session.bulk_save_objects(users)
    session.commit()
    update_redis_data()
    yield _id, _id2


@pytest.yield_fixture()
def one_users(session, test_redis):
    """
    Users have same subnets but only 1 same ip in it
    """
    _id, _id2 = 1, 2
    users = [Users(user_id=_id, ip='1.1.1.1'), Users(user_id=_id, ip='2.2.2.4'),
             Users(user_id=_id2, ip='1.1.1.1'), Users(user_id=_id2, ip='2.2.2.2')]
    session.bulk_save_objects(users)
    session.commit()
    update_redis_data()
    yield _id, _id2


@pytest.yield_fixture()
def simular_users(session, test_redis):
    """
    Users have same subnets with same ips in it - the needed case
    """
    _id, _id2 = 1, 2
    users = [Users(user_id=_id, ip='1.1.1.1'), Users(user_id=_id, ip='2.2.2.2'),
             Users(user_id=_id2, ip='1.1.1.1'), Users(user_id=_id2, ip='2.2.2.2')]
    session.bulk_save_objects(users)
    session.commit()
    update_redis_data()
    yield _id, _id2


@pytest.yield_fixture()
def more_simular(session, test_redis):
    """
    Users have same subnets with same ips in it, even more then needed to
    concider users connected
    """
    _id, _id2 = 1, 2
    users = [Users(user_id=_id, ip='1.1.1.1'), Users(user_id=_id, ip='2.2.2.2'),
             Users(user_id=_id, ip='3.3.3.3'),
             Users(user_id=_id2, ip='1.1.1.1'), Users(user_id=_id2, ip='2.2.2.2'),
             Users(user_id=_id2, ip='3.3.3.3')]
    session.bulk_save_objects(users)
    session.commit()
    update_redis_data()
    yield _id, _id2
