import pytest
from flask import url_for


@pytest.mark.parametrize('http_method, expected_status_code', [
    ('GET', 200),
    ('POST', 405),
    ('PUT', 405),
    ('PATCH', 405),
    ('DELETE', 405),
    ('TRACE', 405),
])
def test_app(client, http_method, expected_status_code):
    r = client.open(url_for('check', u1=0, u2=0), method=http_method)
    assert r.status_code == expected_status_code


def test_count_no_simular(client, simple_users):
    """
    Test if users have same subnets, but no same ips in it
    """
    r = client.get(url_for('check', u1=simple_users[0], u2=simple_users[1]))
    assert r.data == b'0'


def test_count_one_simular(client, one_users):
    """
    Test if users have same subnets, but only 1 same ip in it
    """
    r = client.get(url_for('check', u1=one_users[0], u2=one_users[1]))
    assert r.data == b'1'


def test_count_two_simular(client, simular_users):
    """
    Test if users have common ips in 2 different subnets
    """
    r = client.get(url_for('check', u1=simular_users[0], u2=simular_users[1]))
    assert r.data == b'2'


def test_count_three_simular(client, more_simular):
    """
    Test if users have 3 common ips in 3 different subnets,
    but result still remains two
    """
    r = client.get(url_for('check', u1=more_simular[0], u2=more_simular[1]))
    assert r.data == b'2'
