#!/usr/bin/env bash

export DB="ip"
export PGUSER="demo"
export PGPASSWORD="demo"

export TEST_DB="test_ip"

# count is number of rows inserted in users table
count=100000
# times is quantity of $count batches inserted in one insert function call
times=100

source_dir="$( cd "$( dirname "$0")"/../ ; pwd -P )"

function create() {
sudo -u postgres psql -c "CREATE DATABASE $DB;"
sudo -u postgres psql -d $DB -c "CREATE USER $PGUSER WITH PASSWORD '$PGPASSWORD';
                                GRANT ALL privileges ON DATABASE $DB TO $PGUSER;"

sudo -u postgres psql -c "CREATE DATABASE $TEST_DB;"
sudo -u postgres psql -d $TEST_DB -c "CREATE USER $PGUSER WITH PASSWORD '$PGPASSWORD';
                                      GRANT ALL privileges ON DATABASE $TEST_DB TO $PGUSER;"

$source_dir/venv/bin/python $source_dir/db/models.py

}

function insert() {
    for i in `seq 1 $times`;
    do
        echo "inserting $i batch"
        sudo -u postgres PGPASSWORD=$PGPASSWORD psql -d $DB -U $PGUSER -c "
                   INSERT INTO users
                   SELECT random() * generate_series(1,$count) AS user_id,
                   CONCAT(TRUNC(RANDOM() * 250 + 2), '.',
                          TRUNC(RANDOM() * 250 + 2), '.',
                          TRUNC(RANDOM() * 250 + 2), '.',
                          TRUNC(RANDOM() * 250 + 2))::INET AS $DB;"
    done
}



if [ "$2" -eq "$2" ] 2>/dev/null
then
    times=$2
fi

if [ "$3" -eq "$3" ] 2>/dev/null
then
    count=$3
fi


if [ $1 = "create" ]
then
    create
fi

if [ $1 = "insert" ]
then
    echo "inserting $count rows $times times"
    insert
fi

