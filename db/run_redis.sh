#!/usr/bin/env bash

source_dir="$( cd "$( dirname "$0")"/../ ; pwd -P )"

sudo service redis-server stop

docker pull redis

docker run --name demo-redis -p 127.0.0.1:6379:6379 -v "$source_dir/redislog":/data -d redis redis-server --appendonly yes ||
docker start demo-redis
