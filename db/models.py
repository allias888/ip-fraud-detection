import os
import sys
from sqlalchemy.dialects.postgresql import INET, INTEGER, TIMESTAMP
from sqlalchemy.exc import IntegrityError
from flask_sqlalchemy import SQLAlchemy

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from app import app


db = SQLAlchemy(app)


class session:
    def __enter__(self):
        self.session = db.create_scoped_session()
        return self.session

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.session.remove()


class Users(db.Model):
    """
    Table containing unique (user_id, ip) pairs.
    Rows should only be added in this table, so pairs are unique and table
    support only data insertion
    """
    user_id = db.Column(INTEGER)
    ip = db.Column(INET)
    timestamp = db.Column(TIMESTAMP(timezone=True),
        server_default=db.func.current_timestamp())

    __tablename__ = 'users'
    __table_args__ = (db.PrimaryKeyConstraint('user_id', 'ip'), )

    def __repr__(self):
        return '<user {}, ip {}>'.format(self.user_id, self.ip)


class Updates(db.Model):
    """
    Table containing start and end time of every update.
    In memory data updates track its time and status here
    """
    start = db.Column(db.DateTime)
    end = db.Column(db.DateTime)
    completed = db.Column(db.Boolean, nullable=True)

    __tablename__ = 'updates'
    __table_args__ = (db.PrimaryKeyConstraint('start', 'end'), )

    def __repr__(self):
        return '<start: {}, end {}, completed: {}'.\
            format(self.start, self.end, self.completed)

    def save(self, session):
        try:
            session.add(self)
            session.commit()
        except IntegrityError:
            session.rollback()

    @classmethod
    def last_update(cls, session):
        """
        Returns time and status of last running or completed update
        """
        last_update = session.query(Updates).order_by(Updates.end.desc()).first()

        if not last_update:
            begin = session.query(Users).first().timestamp
            last_update = Updates(start=begin, end=begin, completed=True)
            last_update.save(session)

        return last_update


if __name__ == '__main__':
    db.create_all()
