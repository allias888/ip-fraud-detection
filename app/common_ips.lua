local u1, u2 = ARGV[1], ARGV[2]

local pair = '%s:%s'

local function same_ips_in(subnet)
    local temp_key = string.format("%s-%s-%s", u1, u2, math.random())
    local hll = redis.call('GET', subnet)
    local card_before = redis.call('PFCOUNT', subnet)

    for i = 0, 255 do
        redis.call('SET', temp_key, hll)

        local u1_pair = string.format(pair, u1, i)
        local u2_pair = string.format(pair, u2, i)
        redis.call('PFADD', temp_key, u1_pair, u2_pair)

        local card_after = redis.call('PFCOUNT', temp_key)
        redis.call('DEL', temp_key)

        if card_before == card_after then
            return 1
        end
    end
    return 0
end


local common_sns = redis.call('SINTER', u1, u2)
if not common_sns then
    return 0
end

local c = 0
for _, sn in pairs(common_sns) do
    if c < 2 then
        c = c + same_ips_in(sn)
    end
end

return c
