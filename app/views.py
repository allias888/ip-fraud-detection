from datetime import datetime, timedelta

from app import app, celery, redis, interval
from db.models import Users, Updates, session


@celery.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    """
    Adds periodic task for update redis structures in celery
    """
    sender.add_periodic_task(interval, update.s(interval, 'periodic'))


def update_all():
    """
    Adds task for update structures from the last registered update
    :return:
    """
    with session() as s:
        last_update = Updates.last_update(s)

    new_updates = (datetime.now() - last_update.end).seconds / interval

    for i in range(int(new_updates)):
        update.delay()


@celery.task(ignore_result=True)
def update(seconds=interval, source='delay'):
    """
    Updates probabilistic structures in redis.
    Generates data from user_ids, ips taken in 'seconds' interval
    :param seconds: (int)
    :return: (str)
    """
    with session() as s:
        update_delta = timedelta(seconds=seconds)
        last_update = Updates.last_update(s)

        if last_update.end + update_delta > datetime.now():
            print('updated till now')
            return

        new_update = Updates(completed=False, start=last_update.end,
                             end=last_update.end + update_delta)
        new_update.save(s)
        users = s.query(Users).filter(Users.timestamp.between(new_update.start,
                                                              new_update.end))
        pipe = redis.pipeline()
        for row in users:
            u_id = row.user_id
            octets = row.ip.split('.')

            sn = '{}.{}.{}'.format(*octets[:-1])
            uip_pair = '{}:{}'.format(u_id, octets[-1])

            pipe.sadd(u_id, sn)
            pipe.pfadd(sn, uip_pair)

        pipe.execute()

        new_update.completed = True
        new_update.save(s)
        print(new_update)

    return


@app.route('/check/<u1>/<u2>/', methods=['GET'])
def check(u1, u2):
    """
    Returns number of common ips in different subnets,
    no more than one ip in simular subnet counts

    two users are considered connected, if they have more than two simular ips
    in different /24 subnets

    :param u1: (str) first user id
    :param u2: (str) second user id
    :return: (str) number of different /24 subnets in which users have common ip
    """
    c = redis.evalsha(app.lua_script, 0, u1, u2)
    return str(c)
