from os import popen
from redis import StrictRedis
from flask import Flask
from celery import Celery


def make_celery(app):
    celery = Celery(app.import_name,
                    backend=app.config['CELERY_BACKEND'],
                    broker=app.config['CELERY_BROKER_URL'])
    TaskBase = celery.Task

    class ContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    celery.Task = ContextTask
    return celery


def make_app():
    app = Flask(__name__)
    app.config.update(
        CELERY_BROKER_URL='redis://localhost:6379',
        CELERY_BACKEND='redis://localhost:6379',
        SQLALCHEMY_DATABASE_URI='postgresql://demo:demo@127.0.0.1/ip',
        SQLALCHEMY_TRACK_MODIFICATIONS=True)

    # loading lua script in redis and getting hash to call it later
    cmd = 'redis-cli SCRIPT LOAD "$(cat app/common_ips.lua)"'
    app.lua_script = popen(cmd).read().strip()

    return app

app = make_app()
celery = make_celery(app)
redis = StrictRedis(host='localhost', port=6379, db=0)

# interval of updating data structures in redis (seconds)
interval = 10
