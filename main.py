import argparse
from app.views import app, update_all


parser = argparse.ArgumentParser()
parser.add_argument("-u", "--update",
                    help="update redis placed data from last stored update",
                    action="store_true")
args = parser.parse_args()


if __name__ == "__main__":
    if args.update:
        update_all()
    else:
        app.run()
