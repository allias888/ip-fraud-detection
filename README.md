# IP fraud detection

### Тестовое задание https://gist.github.com/olegvg/c724c4d2022ca48325f0d4ad20ca7ca0

Сервис построен на postgresql, python3, redis, Flask, celery, SQLAlchemy.
Ниже описано как запустить все необходимые компоненты.

## Команды для запуска:

#### 0. Создание виртуального огружения
необходимо для выполнения всех комманд
```
virtualenv --python=python3 venv
source venv/bin/activate
pip install -r requirements.txt
```

#### 1. Создание таблиц и инициализация баз данных:
необходима версия postgres >= 9.3
```bash
bash db/fill_db.sh create
bash db/fill_db.sh insert 10 100000  # вставить 10^5 уникальных пар (user_id, ip) 10 раз
```

#### 2. Запуск Redis
необходима версия redis-server >= 2.8.9
скачать и запустить в контейнере redis-server 3.2 можно коммандой
```bash
bash db/run_redis.sh
```
redis сохраняет дамп в папке redislog/; при рестарте загружает его в память

#### 3. Запуск Celery
Celery используется для выполнения переодических задач по обновелению данных в
Redis, а также для обновления данных с нуля, то есть когда данные в таблице уже есть, 
а сервис не был запущен.
Переодические задачи запускаются автоматически.
```bash
source venv/bin/activate
celery -A app.views.celery beat -l debug  # запуск переодических задач
celery -A app.views.celery --autoscale=1,1 worker  # запуск несколькох обработчиков

celery -A app.views.celery purge  # удалить все задачи
```

#### 4. Запуск сервиса
```python
source venv/bin/activate
python main.py
python main.py --update
```
Интервал обновлений задается в файле app/__init__.py
При запуске загружает lua скрипт в redis.

После запуска celery для рассчета данных за все время стоит воспользоваться
коммандой python main.py --update. При этом на предыдущем шаге можно настроить
количество обработчиков. 

#### 5. Запуск тестов
Тесты можно запускат независимо от предыдущих пунктов
```python
source venv/bin/activate
bash db/fill_db create  # создание бд для тестов

py.test -s -v tests/
```

